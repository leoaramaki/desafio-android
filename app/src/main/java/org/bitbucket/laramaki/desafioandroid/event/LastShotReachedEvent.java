package org.bitbucket.laramaki.desafioandroid.event;

public class LastShotReachedEvent {

    private int lastPage;

    public LastShotReachedEvent(int lastPage) {
        this.lastPage = lastPage;
    }

    public int lastPage() {
        return this.lastPage;
    }
}
