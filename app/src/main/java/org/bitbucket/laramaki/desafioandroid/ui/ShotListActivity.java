package org.bitbucket.laramaki.desafioandroid.ui;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;

import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import org.bitbucket.laramaki.desafioandroid.R;
import org.bitbucket.laramaki.desafioandroid.event.LastShotReachedEvent;
import org.bitbucket.laramaki.desafioandroid.event.ShotsPageFetchedEvent;
import org.bitbucket.laramaki.desafioandroid.model.Page;
import org.bitbucket.laramaki.desafioandroid.model.Shot;
import org.bitbucket.laramaki.desafioandroid.repository.Repository;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class ShotListActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.shots_list)
    RecyclerView shotsList;

    private ShotsAdapter adapter;

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_list);

        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        if (toolbar != null) {
            toolbar.setTitle(R.string.title_activity_shot_list);
            setSupportActionBar(toolbar);
        }

        layoutManager = new LinearLayoutManager(this);

        shotsList.setHasFixedSize(true);
        shotsList.setLayoutManager(layoutManager);
        adapter = new ShotsAdapter(this, 0, 0, null);
        shotsList.setAdapter(adapter);

        Repository.get().fetchPage(1);
    }

    public void onEventMainThread(ShotsPageFetchedEvent event) {
        Page page = event.page();
        int currentPage = Integer.parseInt(page.getPage());

        if (currentPage == adapter.getCurrentPage())
            return;

        SnackbarManager.show(
                Snackbar.with(this)
                        .text(String.format("%s of %d", page.getPage(), page.getPages()))
                        .textTypeface(Typeface.DEFAULT_BOLD)
                        .color(Color.GRAY)
                        .duration(Snackbar.SnackbarDuration.LENGTH_SHORT)
        );

        List<Shot> shots = page.getShots();

        int loadDir = (currentPage < adapter.getCurrentPage()) ?
                ShotsAdapter.LOAD_AT_BEGINNING : ShotsAdapter.LOAD_AT_END;

        adapter.setPerPage(page.getPerPage());
        adapter.setCurrentPage(Integer.parseInt(page.getPage()));

        adapter.addShots(shots, loadDir);
    }

    public void onEventAsync(LastShotReachedEvent event) {
        int nextPage = event.lastPage() + 1;
        Repository.get().fetchPage(nextPage);
    }
}
