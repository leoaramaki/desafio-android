package org.bitbucket.laramaki.desafioandroid.event;

public class FirstShotReachedEvent {

    private final int firstPage;

    public FirstShotReachedEvent(int firstPage) {
        this.firstPage = firstPage;
    }

    public int firstPage() {
        return this.firstPage;
    }
}
