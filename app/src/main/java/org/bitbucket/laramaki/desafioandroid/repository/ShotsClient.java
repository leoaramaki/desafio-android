package org.bitbucket.laramaki.desafioandroid.repository;

import org.bitbucket.laramaki.desafioandroid.model.Page;
import org.bitbucket.laramaki.desafioandroid.model.Shot;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

interface ShotsClient {

    @GET("/shots/popular")
    Page page(
           @Query("page") String page
    );

    @GET("/shots/{id}")
    Shot shotDetails(
            @Path("id") int id
    );

}
