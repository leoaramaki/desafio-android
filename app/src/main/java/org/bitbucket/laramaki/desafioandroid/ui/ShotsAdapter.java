package org.bitbucket.laramaki.desafioandroid.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.bitbucket.laramaki.desafioandroid.R;
import org.bitbucket.laramaki.desafioandroid.event.FirstShotReachedEvent;
import org.bitbucket.laramaki.desafioandroid.event.LastShotReachedEvent;
import org.bitbucket.laramaki.desafioandroid.model.Page;
import org.bitbucket.laramaki.desafioandroid.model.Shot;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

public class ShotsAdapter extends RecyclerView.Adapter<ShotsAdapter.ViewHolder> {

    public static final int LOAD_AT_END = 0x0;

    public static final int LOAD_AT_BEGINNING = 0x1;

    private static final String URI_PLACEHOLDER = "drawable://" + R.drawable.placeholder_shot;

    private CopyOnWriteArrayList<Shot> shots;

    private List<Page> pageList;

    private int currentPage = 1;

    private int perPage = 0;

    private int totalPages;

    private DisplayImageOptions options;

    private WeakReference<Activity> activityRef;

    public ShotsAdapter(Activity activity, int pageNumber, int total, List<Shot> shotList) {
        this.activityRef = new WeakReference<Activity>(activity);
        this.shots = (shotList == null) ? null : new CopyOnWriteArrayList<Shot>(shotList);
        this.totalPages = total;
        this.currentPage = pageNumber;

        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(200))
                .showImageOnLoading(R.drawable.placeholder_shot)
                .build()
                ;

    }

    public void addShots(@NonNull List<Shot> shotsList, int loadDirection) {
        if (shotsList == null)
            throw new IllegalArgumentException("ShotsAdapter: tried to add an empty Shot list");

        if (shots == null) {
            shots = new CopyOnWriteArrayList<Shot>(shotsList);
            notifyItemRangeInserted(0, shotsList.size());
        } else {
            shots.addAll(shotsList);
            notifyItemRangeInserted(shots.size(), shotsList.size());
        }
        Log.d("Count", shots.size() + "");
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getCurrentPage() {
        return this.currentPage;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shot_item, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (position == shots.size() - 1) {
            EventBus.getDefault().post(new LastShotReachedEvent(currentPage));
        }

        holder.textViewShotTitle.setText("untitled");
        holder.textViewViewsCount.setText("0");
        ImageLoader.getInstance().displayImage(URI_PLACEHOLDER, holder.imageViewShot);

        final Shot shot = this.shots.get(position);

        holder.textViewShotTitle.setText(shot.getTitle());
        holder.textViewViewsCount.setText(String.valueOf(shot.getViewsCount()));
        ImageLoader.getInstance().displayImage(shot.getImageUrl(), holder.imageViewShot, options);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.itemView.getContext(), ShotDetailsActivity.class);
                Gson gson = new Gson();
                intent.putExtra(ShotDetailsActivity.EXTRA_DETAIL_JSON, gson.toJson(shot));
                Context context = holder.itemView.getContext();
                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activityRef.get(),
                        new Pair<View, String>(holder.textViewShotTitle, context.getString(R.string.transition_name_title)),
                        new Pair<View, String>(holder.imageViewShot, context.getString(R.string.transition_name_shot))
                );
                ActivityCompat.startActivity(activityRef.get(), intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return (this.shots == null) ? 0 : this.shots.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tv_shot_title)
        TextView textViewShotTitle;

        @Bind(R.id.iv_shot)
        ImageView imageViewShot;

        @Bind(R.id.tv_viewsCount)
        TextView textViewViewsCount;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
