package org.bitbucket.laramaki.desafioandroid.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Page {

    private String page;

    @SerializedName("per_page")
    private int perPage;

    private int pages;

    private int total;

    private List<Shot> shots;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Shot> getShots() {
        return shots;
    }

    public void setShots(List<Shot> shots) {
        this.shots = shots;
    }
}
