package org.bitbucket.laramaki.desafioandroid.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.bitbucket.laramaki.desafioandroid.R;
import org.bitbucket.laramaki.desafioandroid.event.ShotDetailsFetchedEvent;
import org.bitbucket.laramaki.desafioandroid.model.Player;
import org.bitbucket.laramaki.desafioandroid.model.Shot;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;


public class ShotDetailsActivity extends AppCompatActivity {

    static {
        options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(200))
                .showImageOnLoading(R.drawable.placeholder_shot)
                .build()
        ;

        optionsAvatar = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(200))
                .showImageOnLoading(R.drawable.placeholder_avatar)
                .build()
        ;
    }

    public static final String EXTRA_DETAIL_JSON= "detail_json";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.tv_shot_title)
    TextView textViewShotTitle;

    @Bind(R.id.iv_shot)
    ImageView imageViewShot;

    @Bind(R.id.riv_avatar)
    RoundedImageView roundedImageViewAvatar;

    @Bind(R.id.tv_description)
    TextView textViewDescription;

    @Bind(R.id.tv_username)
    TextView textViewUsername;

    @Bind(R.id.tv_viewsCount)
    TextView textViewViewsCount;

    private static DisplayImageOptions options;

    private static DisplayImageOptions optionsAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_details);

        ButterKnife.bind(this);

        EventBus.getDefault().register(this);



        if (toolbar != null) {
            toolbar.setTitle(R.string.title_activity_shot_detail);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (getIntent() != null) {
                    String detailsJson = getIntent().getStringExtra(EXTRA_DETAIL_JSON);

                    Gson gson = new Gson();

                    Shot shot = gson.fromJson(detailsJson, Shot.class);
                    EventBus.getDefault().post(new ShotDetailsFetchedEvent(shot));
                }
            }
        }).start();
    }

    public void onEventMainThread(ShotDetailsFetchedEvent event) {
        Shot shot = event.shot();
        Player player = shot.getPlayer();

        String description = TextUtils.isEmpty(shot.getDescription()) ? "<i>No description available.</i>" : shot.getDescription();
        String title = TextUtils.isEmpty(shot.getTitle()) ? "<i>No title available.</i>" : shot.getTitle();
        String playerName = TextUtils.isEmpty(player.getName()) ? "<i>Name not found</i>" : player.getName();

        textViewDescription.setText(Html.fromHtml(description));
        textViewShotTitle.setText(title);
        textViewUsername.setText(playerName);
        textViewViewsCount.setText(String.valueOf(shot.getViewsCount()));

        ImageLoader.getInstance().displayImage(shot.getImageUrl(), imageViewShot, options, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                Palette palette = Palette.generate(((BitmapDrawable) imageViewShot.getDrawable()).getBitmap(), 40);
                Palette.Swatch swatchToolbar = palette.getVibrantSwatch();
                Palette.Swatch swatchWindowBar = palette.getDarkVibrantSwatch();
                if (swatchToolbar != null) {
                    toolbar.setBackgroundColor(swatchToolbar.getRgb());
                    if (swatchWindowBar != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Window window = getWindow();
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.setStatusBarColor(swatchWindowBar.getRgb());
                    }
                }
            }
        });

        ImageLoader.getInstance().displayImage(shot.getPlayer().getAvatarUrl(), roundedImageViewAvatar, optionsAvatar);
    }
}
