package org.bitbucket.laramaki.desafioandroid.event;

import android.support.annotation.NonNull;

import org.bitbucket.laramaki.desafioandroid.model.Page;

public class ShotsPageFetchedEvent {

    private final Page page;

    public ShotsPageFetchedEvent(@NonNull Page page) {
        this.page = page;
    }

    public Page page() {
        return this.page;
    }
}
