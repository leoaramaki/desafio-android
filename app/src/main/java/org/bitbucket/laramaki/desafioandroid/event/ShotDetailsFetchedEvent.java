package org.bitbucket.laramaki.desafioandroid.event;

import android.support.annotation.NonNull;

import org.bitbucket.laramaki.desafioandroid.model.Shot;

public class ShotDetailsFetchedEvent {

    private final Shot shot;

    public ShotDetailsFetchedEvent(@NonNull Shot shot) {
        this.shot = shot;
    }

    public Shot shot() {
        return this.shot;
    }
}
