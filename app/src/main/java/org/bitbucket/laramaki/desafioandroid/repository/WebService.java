package org.bitbucket.laramaki.desafioandroid.repository;

import android.content.Context;
import android.support.annotation.NonNull;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;

import java.io.File;
import java.io.IOException;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

class WebService {

    // The Dribble API base URL.
    static String API_URL = "http://api.dribbble.com/";

    static RestAdapter restAdapter;

    private WebService() {
    }

    public static void createRestAdapter(Context context) {
        if (restAdapter == null) {
            File responseCacheDir = new File(context.getCacheDir(), "response_cache");

            Cache cache = null;
            cache = new Cache(responseCacheDir, 10 * 1024 * 1024);

            OkHttpClient okHttpClient = new OkHttpClient();
            if (cache != null) {
                okHttpClient.setCache(cache);
            }

            RestAdapter.Builder restBuilder = new RestAdapter.Builder()
                    .setEndpoint(API_URL)
                    .setClient(new OkClient(new OkHttpClient()));

            restAdapter = restBuilder.build();
        }
    }

    public static <S> S createService(@NonNull Class<S> serviceClass) {
        if (restAdapter == null)
            throw new IllegalStateException("WebService: restAdapter is not set");

        return restAdapter.create(serviceClass);
    }

}

