package org.bitbucket.laramaki.desafioandroid.repository;

import android.content.Context;

import org.bitbucket.laramaki.desafioandroid.event.ShotsPageFetchedEvent;
import org.bitbucket.laramaki.desafioandroid.model.Page;

import java.lang.ref.WeakReference;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.greenrobot.event.EventBus;

public class Repository {

    private ExecutorService executorService;

    private static Repository instance;

    private Repository() {
        executorService = Executors.newFixedThreadPool(1);
    }

    public static Repository get() {
        if (instance == null) {
            instance = new Repository();
        }
        return instance;
    }

    public static Repository init(Context context) {
        WebService.createRestAdapter(context);
        return get();
    }

    public void fetchPage(int pageNumber) {
        getPageFromNetwork(pageNumber);
    }

    private void getPageFromNetwork(final int pageNumber) {
        Callable fetchOnNetworkCallable = new Callable<Page>() {

            @Override
            public Page call() throws Exception {
                ShotsClient client = WebService.createService(ShotsClient.class);

                Page page = client.page(String.valueOf(pageNumber));
                EventBus.getDefault().post(new ShotsPageFetchedEvent(page));
                return page;
            }
        };
        executorService.submit(fetchOnNetworkCallable);
    }
}
